from django.apps import AppConfig


class RenAppAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ren_app_app'
